FROM python:3.7-alpine

COPY requirements.txt /
RUN pip3 install -r /requirements.txt

COPY test/. /test
WORKDIR /test/Emulator

ENTRYPOINT ["python3", "-u", "server.py", "-c", "config.json"]