import enum

class IqrfPnum(enum.IntEnum):
    Coordinator = 0
    Node = 1
    OS = 2
    EEPROM = 3
    EEEPROM = 4
    RAM = 5
    LEDR = 6
    LEDG = 7
    SPI = 8
    IO = 9
    Thermometer = 10
    UART = 12
    FRC = 13
    DALI = 74 
    BO = 75
    Sensor = 94
    Light = 113
    Explore = 255
