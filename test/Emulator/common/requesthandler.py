import struct

def parseRequest(request):
    if len(request) < 6:
        return None, None, None, None, None
    request = [request[i:i+1] for i in range(0, len(request), 1)]
    rTok = [struct.unpack("B", request[i])[0] for i in range(0, len(request), 1)]
    nadr = rTok[0]+rTok[1]
    pnum = rTok[2]
    pcmd = rTok[3]
    hwpid = request[4]+request[5]
    if (len(request) > 6):
        pdata = rTok[6:len(request)]
    else:
        pdata = []
    return nadr, pnum, pcmd, hwpid, pdata

def buildResponse(nadr, pnum, pcmd, hwpid, errn, dpaval, pdata):
    data = b''
    nadr = struct.pack("B", nadr) + struct.pack("B", 0)
    pnum = struct.pack("B", pnum)
    pcmd = struct.pack("B", pcmd)
    hwpid = struct.pack("B", hwpid[0]) + struct.pack("B", hwpid[1])
    errn = struct.pack("B", errn)
    dpaval = struct.pack("B", dpaval)
    for i in range(0, len(pdata), 1):
        if type(pdata[i]) == bytes:
            data += pdata[i]
        else:
            data += struct.pack("B", pdata[i])
    return nadr+pnum+pcmd+hwpid+errn+dpaval+data
