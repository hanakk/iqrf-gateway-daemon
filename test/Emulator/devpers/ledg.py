class Ledg:
    def __init__(self):
        self.pert = 7
        self.perte = 3
        self.par1 = 1
        self.par2 = 0
        self.state = 0

    def Set(self, state):
        if state == 1:
            self.state = 1
        else:
            self.state = 0
        return 0, []

    def Pulse(self):
        return 0, []
    
    def Flash(self):
        self.state = 2
        return 0, []

    def GetPerInfo(self):
        ret = []
        ret.append(self.perte)
        ret.append(self.pert)
        ret.append(self.par1)
        ret.append(self.par2)
        return 0, ret

    def HandleRequest(self, cmd):
        if cmd == 0 or cmd == 1:
            return self.Set(cmd)
        elif cmd == 3:
            return self.Pulse()
        elif cmd == 4:
            return self.Flash()
        elif cmd == 63:
            return self.GetPerInfo()
        else:
            return 2, []