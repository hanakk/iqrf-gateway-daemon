class Eeprom:
    def __init__(self):
        self.pert = 3
        self.perte = 3
        self.par1 = 192
        self.par2 = 55
        self.memory = [0] * 192

    def Read(self, nadr, address, length):
        if nadr == 0:
            if 128 <= address < 192:
                if address + length >= 192:
                    return 4, []
                return 0, self.memory[address:address+length]
            else:
                return 4, []

        else:
            if address >= 192:
                return 4, []
            elif address + length > len(self.memory):
                return 4, []
            else:
                return 0, self.memory[address:address+length]
    
    def Write(self, nadr, address, data):
        if nadr == 0:
            if 128 <= address < 192:
                if address + len(data) >= 192:
                    return 4, []
                i = 0
                for j in range(address, address + len(data)):
                    self.memory[j] = data[i]
                    i += 1
            else:
                return 4, []
        else:
            if address >= 192:
                return 4, []
            elif address + len(data) > 192:
                return 4, []
            else:
                i = 0
                for j in range(address, address + len(data)):
                    self.memory[j] = data[i]
                    i += 1
            return 0, []
    
    def GetPerInfo(self):
        ret = []
        ret.append(self.perte)
        ret.append(self.pert)
        ret.append(self.par1)
        ret.append(self.par2)
        return ret

    def HandleRequest(self, nadr, cmd, data):
        if cmd == 0:
            return self.Read(nadr, data[0], data[1])
        elif cmd == 1:
            return self.Write(nadr, data[0], data[1:len(data)])
        if cmd == 63:
            return 0, self.GetPerInfo()
        else:
            return 2, []