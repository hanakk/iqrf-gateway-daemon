class Thermometer:
    def __init__(self, temp):
        self.temp = temp
        self.pert = 11
        self.perte = 1
        self.par1 = 0
        self.par2 = 0

    def Read(self):
        if self.temp >= 0:
            temph = int(self.temp * 16)
            return 0, [int(self.temp), (temph & 0xff), (temph >> 8)]
        else:
            temph = int((256 + self.temp) * 16) 
            return 0, [int(256+self.temp), (temph & 0xff), (temph >> 8)]

    def GetPerInfo(self):
        ret = []
        ret.append(self.perte)
        ret.append(self.pert)
        ret.append(self.par1)
        ret.append(self.par2)
        return ret

    def HandleRequest(self, cmd):
        if cmd == 0:
            return self.Read()
        elif cmd == 63:
            return 0, self.GetPerInfo()
        else:
            return 2, []