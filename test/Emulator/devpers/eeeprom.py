class Eeeprom:
    def __init__(self):
        self.memory = [0] * 32768
        self.pert = 5
        self.perte = 3
        self.par1 = 128
        self.par2 = 0

    def ERead(self, address, length):
        address = int(format(address[1], '008b')+format(address[0],'008b'), 2)
        if address >= 32768:
            return 4, []
        elif length > 54:
            return 4, []
        elif address + length > 32768:
            return 4, []
        else:
            return 0, self.memory[address:(address+length)]
    
    def EWrite(self, address, data):
        address = int(format(address[1], '008b')+format(address[0],'008b'), 2)
        if address >= 16383:
            return 4, []
        elif len(data) > 54:
            return 4, []
        elif address + len(data) > 16383:
            return 4, []
        else:
            i = 0
            for j in range(address, address + len(data)):
                self.memory[j] = data[i]
                i += 1
            return 0, []

    def GetPerInfo(self):
        ret = []
        ret.append(self.perte)
        ret.append(self.pert)
        ret.append(self.par1)
        ret.append(self.par2)
        return ret

    def HandleRequest(self, cmd, data):
        if cmd == 2:
            if len(data) != 3:
                return 5, []
            return self.ERead(data[0:2], data[2])
        elif cmd == 3:
            return self.EWrite(data[0:2], data[2:len(data)])
        elif cmd == 63:
            return 0, self.GetPerInfo()
        else:
            return 2, []