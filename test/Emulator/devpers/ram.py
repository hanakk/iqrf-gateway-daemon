class Ram:
    def __init__(self):
        self.memory = [0] * 48
        self.pert = 5
        self.perte = 3
        self.par1 = 148
        self.par2 = 48
    
    def Read(self, address, length):
        if address >= 48:
            return 4, []
        elif address + length > 48:
            return 4, []
        else:
            return 0, self.memory[address:address+length]
    
    def Write(self, address, data):
        if address >= 48:
            return 4, []
        elif address + len(data) > 48:
            return 4, []
        else:
            i = 0
            for j in range(address, address + len(data)):
                self.memory[j] = data[i]
                i += 1
        return 0, []

    def ReadAny(self):
        return None

    def GetPerInfo(self):
        ret = []
        ret.append(self.perte)
        ret.append(self.pert)
        ret.append(self.par1)
        ret.append(self.par2)
        return ret

    def HandleRequest(self, cmd, data):
        if cmd == 0:
            return self.Read(data[0], data[1])
        elif cmd == 1:
            return self.Write(data[0], data[1:len(data)])
        if cmd == 63:
            return 0, self.GetPerInfo()
        else:
            return 2, []