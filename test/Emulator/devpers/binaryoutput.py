class BinaryOutput:
    def __init__(self, n):
        self.implemented = n
        self.states = [0] * 32

    def SetOutputs(self, outputs, states):  
        outs = []
        count = 0
        for i in outputs:
            tmp = format(i, '008b')
            tmp = tmp[::-1]
            for j in range(8):
                if int(tmp[j]) == 1:
                    count += 1
                outs.append(int(tmp[j]))

        if count != len(states):
            return 6, []

        pS = self.states.copy()
        prevStates = []
        for i in range(4):
            tmp = pS[i*8:(i*8+8)]
            tmp = tmp[::-1]
            tmp = [str(tmp[j]) for j in range(len(tmp))]
            prevStates.append(int('0b'+("".join(tmp)), 2))
  
        k = 0
        for i in range(len(outs)):
            if i == self.implemented:
                break
            else:
                if outs[i] == 1:
                    if states[k] == 0:
                        self.states[i] = 0
                    elif states[k] == 1:
                        self.states[i] = 1
                    elif (2 <= states[k] <= 127) or (129 <= states[k] <= 255):
                        self.states[i] = 0
                    else:
                        return 1, []
                    k += 1
        return 0, prevStates

    def EnumerateOutputs(self):
        return 0, [self.implemented]

    def HandleRequest(self, cmd, data):
        if cmd == 0:
            return self.SetOutputs(data[0:4], data[4:len(data)])
        elif cmd == 62:
            return self.EnumerateOutputs()
        else:
            return 2, []
