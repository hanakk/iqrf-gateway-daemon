class Spi:
    def __init__(self):
        self.pert = 8
        self.perte = 3
        self.par1 = 55
        self.par2 = 0
        self.rx = [-1] * 55
        self.tx = [-1] * 55

    def WriteRead(self, timeout, data):
        if timeout == 255:
            j=0
            for i in data:
                self.rx[j] = data[j]
                j+=1
            self.tx = self.rx.copy()
            return 0, []
        else:
            ret = []
            j=0
            for i in data:
                self.rx[j] = data[j]
                j+=1
            for i in self.tx:
                if i != -1:
                    ret.append(i)
            self.tx = self.rx.copy()
            return 0, ret

    def GetPerInfo(self):
        ret = []
        ret.append(self.perte)
        ret.append(self.pert)
        ret.append(self.par1)
        ret.append(self.par2)
        return ret

    def HandleRequest(self, cmd, data):
        if cmd == 0:
            return self.WriteRead(data[0], data[1:len(data)])
        elif cmd == 63:
            return 0, self.GetPerInfo()
        else:
            return 2, []