class Device:
    def __init__(self, hwpid, dpaval, mid, ibk):
        self.hwpid = hwpid
        self.dpaval = dpaval
        self.mid = mid
        self.ibk = ibk
        self.embedpers = dict()
        self.stdpers = dict()
