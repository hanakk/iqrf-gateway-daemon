def Enumerate(network, addr):
    if str(addr) in network:
        epers = [0] * 32 
        pers = network[str(addr)].embedpers
        dpaver = pers['2'].dpaver
        for key in pers:
            epers[int(key)] = 1
        retpers = []
        for i in range(4): 
            tmp = epers[i*8:(i*8+8)]
            tmp = tmp[::-1]
            tmp = [str(tmp[j]) for j in range(len(tmp))] 
            retpers.append(int('0b'+("".join(tmp)), 2))
        ret = [dpaver, 0]
        ret.extend(retpers)
        ret.append(b'\x00\x00\x00\x00\x01')
        return 0, ret
    return 1, []
    
def GetMorePerInfo(network, addr, first):
    if str(addr) in network:
        dev = network[str(addr)]
        ret = []
        for i in range(first, 14):
            if str(i) in dev.embedpers:
                ret.append(dev.embedpers[str(i)].perte)
                ret.append(dev.embedpers[str(i)].pert)
                ret.append(dev.embedpers[str(i)].par1)
                ret.append(dev.embedpers[str(i)].par2)
            else:
                ret.extend([3,0,0,0])
        while ret[len(ret-4):len(ret)] == [3,0,0,0]:
            ret = ret[0:len(ret)-4]
        return 0, ret
    else:
        return 1, []

def HandleRequest(network, addr, cmd):
    if 0 <= cmd <= 14:
        return GetMorePerInfo(network, addr, cmd)
    elif cmd == 63:
        return Enumerate(network, addr)
    else:
        return 2, []

