class Coordinator:
    def __init__(self, did):
        self.did = did
        self.discovered = [0] * 256
        self.bonded = [0] * 256
        self.mids = dict()
        self.rqhops = 255
        self.rsphops = 255
        self.pert = 1
        self.perte = 3
        self.par1 = 56
        self.par2 = 0
        self.network_data = [16, 121, 146, 138, 36, 179, 60, 4, 230, 44, 153, 177, 197, 61, 87, 210, 180, 131, 226, 181, 23, 1, 21, 176, 209, 24, 61, 237, 237, 106, 169, 130, 145, 21, 228, 142, 203, 169, 223, 62, 157, 52, 134, 86, 123, 143, 197, 76, 168]

    def countBonded(self):
        count = 0
        for i in range(len(self.bonded)):
            if self.bonded[i] == 1:
                count += 1
        return count

    def AddrInfo(self, network):
        return 0, [self.countBonded(), self.did], network
    
    def DiscoveredNodes(self, network):
        ret = []
        for i in range(32):
            tmp = self.discovered[i*8:(i*8+8)]
            tmp = tmp[::-1]
            tmp = [str(tmp[j]) for j in range(len(tmp))]
            ret.append(int('0b'+("".join(tmp)), 2))
        return 0, ret, network
            
    def BondedNodes(self, network):       
        ret = []
        for i in range(32):
            tmp = self.bonded[i*8:(i*8+8)]
            tmp = tmp[::-1]
            tmp = [str(tmp[j]) for j in range(len(tmp))]
            ret.append(int('0b'+("".join(tmp)), 2))
        return 0, ret, network
    
    def ClearBonds(self, network):
        self.bonded = [0] * 256
        self.discovered = [0] * 256
        return 0, [], network
    
    def BondNode(self, network, addr):
        if addr == 0:
            for i in range (1, 240):
                if self.bonded[i] == 0:
                    self.bonded[i] = 1
                    return 0, [i, self.countBonded()], network
            return 1, [], network     
        elif addr >= 240 or addr < 0:
            return 1, [], network
        else:
            if self.bonded[addr] == 1:
                return 1, [], network
            else:
                self.bonded[addr] = 1
                return 0, [addr, self.countBonded()], network
    
    def RemoveBond(self, network, addr):
        if 0 <= addr < 240:
            if self.bonded[addr] == 1:
                self.bonded[addr] = 0
                self.discovered[addr] = 0
                return 0, [], network
            else:
                return 1, [], network
        else:
            return 1, [], network
    
    def Discovery(self, network, maxaddr):
        count = 0
        for i in range (1, maxaddr):
            if str(i) in network:
                self.discovered[i] = 1
                count += 1
        return 0, count, network

    def SetDpa(self, network, dpaparam):
        pdpaparam = network['0'].dpaval
        network['0'].dpaval = dpaparam
        return 0, pdpaparam, network

    def SetHops(self, network, rqhops, rsphops):
        prq = self.rqhops
        prsp = self.rsphops
        self.rqhops = rqhops
        self.rsphops = rsphops
        return 0, [prq, prsp], network
    
    def Backup(self, network, index):
        ret= []
        if index > 49:
            return 6, ret, network
        else:
            for i in range(index, len(self.network_data)):
                ret.append(self.network_data[i])

        return 0, ret, network
    
    def Restore(self, network, data):
        if len(data) != 49:
            return 6, [], network

        self.network_data = []
        for i in range(0, len(data)):
            self.network_data.append(data[i])

        return 0, [], network
    
    def AuthorizeBond(self, network, addr, mid):
        return None
    
    def SmartConnect(self, network, addr, bonding_test, ibk, mid, res0, virtual_address, res1, data):
        return None

    def SetMID(self, network, mid, addr):
        self.mids[str(addr)] = mid[::-1]

        return 0, [], network

    def GetPerInfo(self):
        ret = []
        ret.append(self.perte)
        ret.append(self.pert)
        ret.append(self.par1)
        ret.append(self.par2)
        return ret
    
    def HandleRequest(self, network, cmd, data):
        if cmd == 0:
            return self.AddrInfo(network)
        elif cmd == 1:
            return self.DiscoveredNodes(network)
        elif cmd == 2:
            return self.BondedNodes(network)
        elif cmd == 3:
            return self.ClearBonds(network)
        elif cmd == 4:
            return self.BondNode(network, data[0])
        elif cmd == 5:
            return self.RemoveBond(network, data[0])
        elif cmd == 7:
            return self.Discovery(network, data[1])
        elif cmd == 8:
            return self.SetDpa(network, data[0])
        elif cmd == 9:
            return self.SetHops(network, data[0], data[1])
        elif cmd == 11:
            return self.Backup(network, data[0])
        elif cmd == 12:
            return self.Restore(network, data[0:len(data)])
        elif cmd == 13:
            return self.AuthorizeBond(network, data[0], data[1:len(data)])
        elif cmd == 18:
            return self.SmartConnect(network, data[0], data[1], data[2:18], data[18:22], data[22:24], data[24], data[25:34], data[34:len(data)])
        elif cmd == 19:
            return self.SetMID(network, data[0:4], data[4])
        elif cmd == 63:
            return 0, self.GetPerInfo(), network
        else:
            return 2, [], network