class Io:
    def __init__(self, data):
        self.direction = [0] * 5
        self.pullup = [0] * 3
        self.outputs = [0] * 5
        self.data = data
        self.pert = 9
        self.perte = 3
        self.par1 = 7
        self.par2 = 0

    def Direction(self, data):
        if len(data) % 3 != 0:
            return 5, []
        for i in range(0, len(data), 3):
            if data[i] not in [0,1,2,17]:
                return 6, []
        for i in range(0, len(data), 3):
            port, mask, value = data[i], data[i+1], data[i+2]
            if 0 <= port < 3:
                self.direction[port] = 0xff & mask & value
                return 0, []
            elif port == 17:
                self.pullup[1] = 0xff & mask & value
                return 0, []

    def Set(self, data):
        if len(data) % 3 != 0:
            return 5, []
        for i in range(0, len(data), 3):
            cmd0, cmd1, cmd2 = data[i], data[i+1], data[i+2]
            if 0 <= cmd0 < 3:
                self.outputs[cmd0] = not(self.direction[cmd0]) & cmd1 & cmd2
            elif cmd0 == 255:
                # set delay, but response is sent immediately
                continue
            else:
                break
        return 0, []
                
    def Get(self):
        ret = []
        for i in self.direction:
            ret.append(self.data & i)
        return 0, ret

    def GetPerInfo(self):
        ret = []
        ret.append(self.perte)
        ret.append(self.pert)
        ret.append(self.par1)
        ret.append(self.par2)
        return ret

    def HandleRequest(self, cmd, data):
        if cmd == 0:
            return self.Direction(data)
        elif cmd == 1:
            return self.Set(data)
        elif cmd == 2:
            return self.Get()
        elif cmd == 63:
            return 0, self.GetPerInfo()
        else:
            return 2, []