import enum

class SType(enum.IntEnum):
    Temp = 1
    CO2 = 2
    VOC = 3

class Sensor:
    def __init__(self):
        self.sensors = dict()

    def ReadS(self, i):
        if self.sensors[i][0] == SType.Temp:
            return [0, 0]
        elif self.sensors[i][0] == SType.CO2:
            return [self.sensors[i][1] & 0xff, (self.sensors[i][1] >> 8) & 0xff]
        elif self.sensors[i][0] == SType.VOC:
            return [self.sensors[i][1] & 0xff, (self.sensors[i][1] >> 8) & 0xff]

    def ReadSType(self, i):
        ret = [self.sensors[i][0]]
        ret.extend(self.ReadS(i))
        return ret
    
    def Read(self, types, sensors, data):
        ret = []
        sensorlist = []
        for i in sensors:
            tmp = format(i, '008b')
            tmp = tmp[::-1]
            for j in range(8):
                sensorlist.append(int(tmp[j]))

        if types == 0:
            for i in range(len(sensorlist)):
                if sensorlist[i] == 1 and i in self.sensors:
                    ret.extend(self.ReadS(i))
        else:
            for i in range(len(sensorlist)):
                if sensorlist[i] == 1 and i in self.sensors:
                    ret.extend(self.ReadSType(i))
        return 0, ret

    def Enumerate(self):
        ret = []
        for i in range(len(self.sensors)):
            ret.append(self.sensors[i][0])
        return 0, ret

    def HandleRequest(self, cmd, data):
        if cmd == 0:
            if len(data) == 0:
                return 0, [self.ReadS(0)]
            elif len(data) == 4:
                return self.Read(0, data, [])
            else:
                if len(data) % 5 == 4:
                    return self.Read(0, data[0:4], data[4:len(data)])
                else:
                    return 5, []
        elif cmd == 1:
            if len(data) == 0:
                return 0, [self.ReadSType(0)]
            elif len(data) == 4:
                return self.Read(1, data, [])
            else:
                if len(data) % 5 == 4:
                    return self.Read(1, data[0:4], data[4:len(data)])
                else:
                    return 5, []
        elif cmd == 62:
            return self.Enumerate()
        else:
            return 2, []