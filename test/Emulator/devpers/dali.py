class Dali:
    def __init__(self, hwpid, dali):
        self.hwpid = hwpid
        self.dali = dali

    def DaliCommands(self, dali):
        return 0, 76

    def DaliCommandsAsynchronously(self, dali):
        return 0, 76


    def HandleRequest(self, cmd, data):
        if cmd == 0:
            return self.DaliCommands(data)
        elif cmd == 1:
            return self.DaliCommandsAsynchronously(data)
        else:
            return 2, []