class Uart:
    def __init__(self):
        self.pert = 12
        self.perte = 3
        self.par1 = 55
        self.par2 = 0
        self.tx = [-1] * 64
        self.rx = [-1] * 64
        self.baudrate = -1

    def OpenUart(self, baudrate):
        if 0 <= baudrate <= 8:
            self.baudrate = baudrate
            return 0, []
        else:
            return 6, []

    def CloseUart(self):
        self.baudrate = -1
        return 0, []

    def WriteRead(self, timeout, data):
        if self.baudrate == -1:
            return 1, []
        else:
            if timeout == 255:
                for i in range(len(data)):
                    self.rx[i] = data[i]
                self.tx = self.rx.copy()
                return 0, []
            else:
                ret = []
                for i in range(len(data)):
                    self.rx[i] = data[i]
                for i in self.tx:
                    if i != -1:
                        ret.append(i)
                self.tx = self.rx.copy()
                return 0, ret

    def ClearWriteRead(self, timeout, data):
        self.rx = [-1] * 64
        return self.WriteRead(timeout, data)

    def GetPerInfo(self):
        ret = []
        ret.append(self.perte)
        ret.append(self.pert)
        ret.append(self.par1)
        ret.append(self.par2)
        return ret

    def HandleRequest(self, cmd, data):
        if cmd == 0:
            return self.OpenUart(data[0])
        elif cmd == 1:
            return self.CloseUart()
        elif cmd == 2:
            return self.WriteRead(data[0], data[1:len(data)])
        elif cmd == 3:
            return self.ClearWriteRead(data[0], data[1:len(data)])
        elif cmd == 63:
            return 0, self.GetPerInfo()
        else:
            return 2, []