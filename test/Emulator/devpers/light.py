class Light:
    def __init__(self):
        self.power = [0] * 32
    
    def SetPower(self, lights, power):
        ligh = []
        count = 0
        for i in lights:
            # tmp = format(i, '008b')
            tmp = bin(i)
            tmp = [int(tmp[j]) for j in range(2, len(tmp), 1)]
            if len(tmp) < 8:  #
                for j in range(8 - len(tmp)):
                    tmp.insert(0, 0)

            tmp = tmp[::-1]
            for j in range(8):
                if int(tmp[j]) == 1:
                    count += 1
                ligh.append(int(tmp[j]))

        if count != len(power):
            return 6, []

        ps = self.power.copy()
        prevstates = []
        for i in range(4):
            tmp = ps[i * 8:(i * 8 + 8)]
            tmp = tmp[::-1]
            tmp = [str(tmp[j]) for j in range(len(tmp))]
            prevstates.append(int('0b' + ("".join(tmp)), 2))

        k = 0
        for i in range(len(ligh)):
            if ligh[i] == 1:
                if 0 <= power[k] <= 10:
                    self.power[i] = 10
                elif power[k] == 127:
                    pass
                else:
                    return 1, []
                k += 1

        return 0, prevstates

    def IncrementPower(self, lights, power):
        ligh = []
        count = 0
        for i in lights:
            tmp = bin(i)
            tmp = [int(tmp[j]) for j in range(2, len(tmp), 1)]
            if len(tmp) < 8:  #
                for j in range(8 - len(tmp)):
                    tmp.insert(0, 0)
            tmp = tmp[::-1]
            for j in range(8):
                if tmp[j] == 1:
                    count += 1
                ligh.append(tmp[j])

        if count != len(power):
            return 6, []

        ps = self.power.copy()
        prevstates = []
        for i in range(4):
            tmp = ps[i * 8:(i * 8 + 8)]
            tmp = tmp[::-1]
            tmp = [str(tmp[j]) for j in range(len(tmp))]
            prevstates.append(int('0b' + ("".join(tmp)), 2))

        k = 0
        for i in range(len(ligh)): # upravit nechapem presne dokumentacii
            if ligh[i] == 1:
                if 0 <= power[k] <= 10:
                    if self.power[i] + power[k] <= 10:
                        self.power[i] = self.power[i] + power[k]
                    else:
                        self.power[i] = 10
                elif power[k] == 127:
                    pass
                else:
                    return 1, []
                k += 1

        return 0, prevstates

    def DecrementPower(self, lights, power):
        ligh = []
        count = 0
        for i in lights:
            tmp = bin(i)
            tmp = [int(tmp[j]) for j in range(2, len(tmp), 1)]
            if len(tmp) < 8:
                for j in range(8 - len(tmp)):
                    tmp.insert(0, 0)
            tmp = tmp[::-1]
            for j in range(8):
                if tmp[j] == 1:
                    count += 1
                ligh.append(tmp[j])

        if count != len(power):
            return 6, []

        ps = self.power.copy()
        prevstates = []
        for i in range(4):
            tmp = ps[i * 8:(i * 8 + 8)]
            tmp = tmp[::-1]
            tmp = [str(tmp[j]) for j in range(len(tmp))]
            prevstates.append(int('0b' + ("".join(tmp)),
                                  2))

        k = 0
        for i in range(len(ligh)): # upravit nechapem presne dokumentacii
            if ligh[i] == 1:
                if 0 <= power[k] <= 10:
                    if self.power[i] - power[k] >= 0:
                        self.power[i] = self.power[i] - power[k]
                    else:
                        self.power[i] = 0
                elif power[k] == 127:
                    pass
                else:
                    return 1, []
                k += 1

        return 0, prevstates
    
    def EnumerateLights(self):
        ret = int(len(self.power))
        return 0, [ret]


    def HandleRequest(self, cmd, data):
        if cmd == 0:
            return self.SetPower(data[0:4], data[4:len(data)])
        elif cmd == 1:
            return self.IncrementPower(data[0:4], data[4:len(data)])
        elif cmd == 2:
            return self.DecrementPower(data[0:4], data[4:len(data)])
        elif cmd == 62:
            return self.EnumerateLights()
        else:
            return 2, []