class Node:
    def __init__(self, bonded):
        self.bonded = bonded
        self.pert = 2
        self.perte = 3
        self.par1 = 56
        self.par2 = 0
        self.network_data = [16, 139, 220, 162, 99, 237, 240, 96, 17, 35, 77, 205, 28, 174, 177, 39, 180, 131, 226, 181, 23, 1, 21, 176, 209, 24, 61, 237, 237, 106, 169, 130, 145, 21, 228, 142, 203, 169, 223, 62, 157, 52, 134, 86, 123, 143, 197, 76, 6]

    def ReadN(self):
        return None

    def RemoveBond(self, network, addr):
        network[str(addr)].embedpers[str(1)].bonded = 0
        return 0, [], network

    def BackupN(self, network, index):
        ret = []
        if index > 49:
            return 6, ret, network
        else:
            for i in range(index, len(self.network_data)):
                ret.append(self.network_data[i])

        return 0, ret, network

    def RestoreN(self, network, data):
        if len(data) != 49:
            return 6, [], network

        self.network_data = []
        for i in range(0, data):
            self.network_data.append(data[i])

        return 0, [], network

    def ValidateBonds(self):
        return None
    
    def GetPerInfo(self):
        ret = []
        ret.append(self.perte)
        ret.append(self.pert)
        ret.append(self.par1)
        ret.append(self.par2)
        return ret
    
    def HandleRequest(self, network, addr, cmd, data):
        if cmd == 0:
            return 0, self.ReadN(), network
        elif cmd == 1:
            return self.RemoveBond(network, addr)
        elif cmd == 6:
            return self.BackupN(network, data[0])
        elif cmd == 7:
            return self.RestoreN(network, data[0:len(data)])
        elif cmd == 8:
            return 0, [], network
        elif cmd == 63:
            return 0, self.GetPerInfo(), network
        else:
            return 2, [], network
    