from devpers import explore

class Os:
    def __init__(self):
        self.pert = 3 
        self.perte = 3
        self.par1 = 22
        self.par2 = 131
        self.dpaver = b'\x13\x04'
        self.security = [0] * 16

    def ReadOs(self, network, addr):
        perenum = explore.Enumerate(network, addr)[1]
        ret = []
        ret.append(b'\x2d\x1d\x11\x81\x43\xb4\xc8\x08\x00\x31\x20\x75')
        ret.extend(b'\x74\x0e\x2d\xde\x6f\xab\x5a\x1f\xb1\xf3\x96\x33\xd6\x20\x06\x1e')
        ret.extend(perenum)
        return 0, ret, network

    def Batch(self):
        return None

    def SelectiveBatch(self):
        return None

    def Reset(self, network, addr):
        # execute reset routines
        return 0, [], network

    def Restart(self, network, addr):
        # execute restart routines
        return 0, [], network

    def RunRfpgm(self):
        return None

    def SetSecurity(self, network, type, data):
        if type == 0 or type == 1:
            self.security = data
            return 0, [], network
        else:
            return 6, [], network 

    def Sleep(self):
        return None

    def LoadCode(self):
        return None

    def ReadCfg(self):
        return None

    def WriteCfg(self):
        return None

    def WriteCfgByte(self):
        return None

    def TestRfSigna(self):
        return None

    def FactorySettings(self):
        return None
    
    def GetPerInfo(self):
        ret = []
        ret.append(self.perte)
        ret.append(self.pert)
        ret.append(self.par1)
        ret.append(self.par2)
        return ret

    def HandleRequest(self, network, addr, cmd, data):
        if cmd == 0:
            return self.ReadOs(network, addr)
        elif cmd == 1:
            return self.Reset(network, addr)
        elif cmd == 6:
            return self.SetSecurity(network, data[0], data[1:16])
        elif cmd == 8:
            return self.Restart(network, addr)
        elif cmd == 63:
            return 0, self.GetPerInfo(), network
        else:
            return 2, [], network