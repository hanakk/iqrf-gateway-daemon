#!/usr/bin/env python

import socket, time, argparse, json, pprint, os
import struct
from common import requesthandler as rh
from common.iqrfPnum import IqrfPnum
from devpers import device, ledr, ledg, coordinator, node, os, binaryoutput, dali, thermometer, \
    explore, spi, eeprom, uart, ram, eeeprom, light, io, sensor

network = dict()
nadr_array = []
address = '0.0.0.0'
portnum = 10000
BUFFER_SIZE = 1024
sockfd = None

parser = argparse.ArgumentParser()
parser.add_argument('-c', type=str, required=True)
parser.add_argument('-f')
args = parser.parse_args()

with open(args.c) as file:
    data = json.load(file)
file.close()

for item in data:
    dev = data[item]
    network[dev['nadr']] = device.Device(dev['hwpid'], int(dev['dpaval']), int(dev['mid']), [])
    # always enabled peripherals
    if int(dev['nadr']) == IqrfPnum.Coordinator:
        network[dev['nadr']].embedpers['0'] = coordinator.Coordinator(int(dev['did']))
    else:
        network[dev['nadr']].embedpers['1'] = node.Node(int(dev['bonded']))
        ibk_array = []
        for ibk in dev['ibk']:
            ibk_array.append(ibk)
        network[dev['nadr']].ibk = ibk_array
    network[dev['nadr']].embedpers['2'] = os.Os()

    nadr_array.append(dev['nadr'])
    if len(nadr_array) != len(set(nadr_array)):
        print("nadr value is not unique")
        exit(1)

    # optional peripherals
    for per in dev['per']:
        if int(per['pnum']) == IqrfPnum.EEPROM:
            network[dev['nadr']].embedpers[per['pnum']] = eeprom.Eeprom()
        elif int(per['pnum']) == IqrfPnum.EEEPROM:
            network[dev['nadr']].embedpers[per['pnum']] = eeeprom.Eeeprom()
        elif int(per['pnum']) == IqrfPnum.RAM:
            network[dev['nadr']].embedpers[per['pnum']] = ram.Ram()
        elif int(per['pnum']) == IqrfPnum.LEDR:
            network[dev['nadr']].embedpers[per['pnum']] = ledr.Ledr()
        elif int(per['pnum']) == IqrfPnum.LEDG:
            network[dev['nadr']].embedpers[per['pnum']] = ledg.Ledg()
        elif int(per['pnum']) == IqrfPnum.SPI:
            network[dev['nadr']].embedpers[per['pnum']] = spi.Spi()
        elif int(per['pnum']) == IqrfPnum.IO:
            network[dev['nadr']].embedpers[per['pnum']] = io.Io(int(per['data']))
        elif int(per['pnum']) == IqrfPnum.Thermometer:
            network[dev['nadr']].embedpers[per['pnum']] = thermometer.Thermometer(float(per['temp']))
        elif int(per['pnum']) == IqrfPnum.UART:
            network[dev['nadr']].embedpers[per['pnum']] = uart.Uart()
        elif int(per['pnum']) == IqrfPnum.DALI:
            network[dev['nadr']].stdpers[per['pnum']] = dali.Dali(dev['hwpid'], per['state'])
        elif int(per['pnum']) == IqrfPnum.BO:
            network[dev['nadr']].stdpers[per['pnum']] = binaryoutput.BinaryOutput(per['inputs'])
        elif int(per['pnum']) == IqrfPnum.Sensor:
            network[dev['nadr']].stdpers[per['pnum']] = sensor.Sensor()
            i = 0
            for s in per['sensors']:
                network[dev['nadr']].stdpers[per['pnum']].sensors[i] = [int(s['type']), int(s['value'])]
                i += 1
        elif int(per['pnum']) == IqrfPnum.Light:
            network[dev['nadr']].stdpers[per['pnum']] = light.Light()

def processRequest(network, request):
    rqNadr, rqPnum, rqCmd, rqHwpid, rqPdata = rh.parseRequest(request)
    errn = 0
    if str(rqNadr) in network:
        rqHwpid = [0,0]
        if rqNadr != 0 and network[str(rqNadr)].embedpers[str(1)].bonded != 1:
            errn, rqPdata = 8, []
        if str(rqPnum) in network[str(rqNadr)].embedpers:
            peripheral = network[str(rqNadr)].embedpers[str(rqPnum)]
            if rqPnum == IqrfPnum.Coordinator:
                errn, rqPdata, network = peripheral.HandleRequest(network, rqCmd, rqPdata)
            elif rqPnum == IqrfPnum.Node:
                errn, rqPdata, network = peripheral.HandleRequest(network, rqNadr, rqCmd, rqPdata)
            elif rqPnum == IqrfPnum.OS:
                errn, rqPdata, network = peripheral.HandleRequest(network, rqNadr, rqCmd, rqPdata)
            elif rqPnum == IqrfPnum.EEPROM:
                errn, rqPdata = peripheral.HandleRequest(rqNadr, rqCmd, rqPdata)
            elif rqPnum == IqrfPnum.EEEPROM:
                errn, rqPdata = peripheral.HandleRequest(rqCmd, rqPdata)
            elif rqPnum == IqrfPnum.RAM:
                errn, rqPdata = peripheral.HandleRequest(rqCmd, rqPdata)
            elif rqPnum == IqrfPnum.LEDR:
                errn, rqPdata = peripheral.HandleRequest(rqCmd)
            elif rqPnum == IqrfPnum.LEDG:
                errn, rqPdata = peripheral.HandleRequest(rqCmd)
            elif rqPnum == IqrfPnum.SPI:
                errn, rqPdata = peripheral.HandleRequest(rqCmd, rqPdata)
            elif rqPnum == IqrfPnum.IO:
                errn, rqPdata = peripheral.HandleRequest(rqCmd, rqPdata)
            elif rqPnum == IqrfPnum.Thermometer:
                errn, rqPdata = peripheral.HandleRequest(rqCmd)
            elif rqPnum == IqrfPnum.UART:
                errn, rqPdata = peripheral.HandleRequest(rqCmd, rqPdata)
        elif str(rqPnum) in network[str(rqNadr)].stdpers:
            peripheral = network[str(rqNadr)].stdpers[str(rqPnum)]
            if rqPnum == IqrfPnum.DALI:
                errn, rqPdata = peripheral.HandleRequest(rqCmd, rqPdata)
            elif rqPnum == IqrfPnum.BO:
                rqHwpid = [network[str(rqNadr)].hwpid & 0xff, (network[str(rqNadr)].hwpid >> 8) & 0xff]
                errn, rqPdata = peripheral.HandleRequest(rqCmd, rqPdata)
            elif rqPnum == IqrfPnum.Sensor:
                rqHwpid = [network[str(rqNadr)].hwpid & 0xff, (network[str(rqNadr)].hwpid >> 8) & 0xff]
                errn, rqPdata = peripheral.HandleRequest(rqCmd, rqPdata)
            elif rqPnum == IqrfPnum.Light:
                rqHwpid = [network[str(rqNadr)].hwpid & 0xff, (network[str(rqNadr)].hwpid >> 8) & 0xff]
                errn, rqPdata = peripheral.HandleRequest(rqCmd, rqPdata)
        elif rqPnum == IqrfPnum.Explore:
            errn, rqPdata = explore.HandleRequest(network, rqNadr, rqCmd)
        else:
            if rqPnum == IqrfPnum.EEEPROM:
                errn, rqPdata = 1, []
            elif rqPnum == IqrfPnum.Thermometer:
                errn, rqPdata = 0, [128, 0, 0]
            else:
                errn, rqPdata = 1, []
    else:
        errn, rqPdata = 8, []
    response = rh.buildResponse(rqNadr, rqPnum, rqCmd + 128, rqHwpid, errn, network[str(rqNadr)].dpaval, rqPdata)
    return response

ret = processRequest(network, b'\x01\x00\x02\x01\xff\xff')

def sendConfig(conneciton):
    time.sleep(0.1)
    configstring = b'\x00\x00\xff\x3f\x00\x00\x80\x00\x13\x04\x00\xfd\x22\x00\x00\x00\x00\x00\x00\x05'
    conneciton.send(configstring)

try:
    sockfd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error as msg:
    print("error socket")
    sockfd.close()

try:
    sockfd.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
except socket.error as msg:
    print("error setsockopt")
    sockfd.close()

try:
    sockfd.bind((address, portnum))
except socket.error as msg:
    print("error bind")
    sockfd.close()

try:
    sockfd.listen(1)
except socket.error as msg:
    print("error listen")
    sockfd.close()

print("Awaiting client connection")

connection, addr = sockfd.accept()
print(addr)
sendConfig(connection)
while 1:
    request = connection.recv(BUFFER_SIZE)
    print('received request')
    print(request)
    if len(request) == 0:
        continue
    response = processRequest(network, request)
    print(response)
    connection.send(response)
    print("Response sent")
connection.close()
