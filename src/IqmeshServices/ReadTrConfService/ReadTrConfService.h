#pragma once

#include "IReadTrConfService.h"
#include "ShapeProperties.h"
#include "IIqrfDpaService.h"
#include "IMessagingSplitterService.h"
#include "ITraceService.h"
#include <string>


namespace iqrf {

  /// \class ReadTrConfService
  /// \brief Implementation of IReadTrConfService
  class ReadTrConfService : public IReadTrConfService
  {
  public:
    /// \brief Constructor
    ReadTrConfService();

    /// \brief Destructor
    virtual ~ReadTrConfService();

    void activate(const shape::Properties *props = 0);
    void deactivate();
    void modify(const shape::Properties *props);

    void attachInterface(iqrf::IIqrfDpaService* iface);
    void detachInterface(iqrf::IIqrfDpaService* iface);

    void attachInterface(IMessagingSplitterService* iface);
    void detachInterface(IMessagingSplitterService* iface);

    void attachInterface(shape::ITraceService* iface);
    void detachInterface(shape::ITraceService* iface);

  private:
    class Imp;
    Imp* m_imp;
  };
}
